{-# OPTIONS_GHC -fno-warn-orphans #-}
{-# LANGUAGE ConstraintKinds         #-}
{-# LANGUAGE DataKinds               #-}
{-# LANGUAGE FlexibleContexts        #-}
{-# LANGUAGE FlexibleInstances       #-}
{-# LANGUAGE GADTs                   #-}
{-# LANGUAGE PolyKinds               #-}
{-# LANGUAGE RankNTypes              #-}
{-# LANGUAGE ScopedTypeVariables     #-}
{-# LANGUAGE StaticPointers          #-}
{-# LANGUAGE TypeApplications        #-}
{-# LANGUAGE UndecidableSuperClasses #-}

module Hyperion.Static.Reflection
  ( withClosureDict
  ) where

import           Control.Distributed.Static (Closure)
import           Data.Constraint            (Dict (..))
import           Hyperion.Static.Binary     ()
import           Hyperion.Static.CPure      (cPure)
import           Hyperion.Static.Class      (Static (..), cAp, ptrAp)
import           Hyperion.Static.Typeable   ()
import           Type.Reflection            (Typeable)
import           Unsafe.Coerce              (unsafeCoerce)

-- * Reflection
--
-- We provide a function 'withClosureDict' for constructing instances
-- of 'Static c' "on the fly", inspired by the reflection
-- package. Doing this requires a little hackery with
-- 'unsafeCoerce'. See
-- https://stackoverflow.com/questions/17793466/black-magic-in-haskell-reflection
-- for an explanation of the basic trick. The situation in our case is
-- different because 'Static c' has a superclass constraint, which
-- modifies how GHC structures the underlying dictionary. A discussion
-- of how to modify the trick for our use case is here:
-- https://www.reddit.com/r/haskell/comments/mv1zx2/implementing_reifiesgiven_for_class_with/

-- | A concrete datatype that should be isomorphic to GHCs internal
-- representation of the dictionary for 'Static c'.
data StaticClass c where
  MkStaticClass :: c => Closure (Dict c) -> StaticClass c

-- | A newtype for guiding GHC, exactly as in the 'reflection' package.
newtype Magic c r = MkMagic (Static c => r)

-- | In the underlying core, 'Magic c r' is the same as 'Static c =>
-- r' (because Magic is a newtype), and 'Static c => r' is the same as
-- 'StaticDict c -> r' where 'StaticDict c' is GHCs internal
-- dictionary for 'Static c'. Finally, since 'StaticClass c' is
-- (hopefully) the same as 'StaticDict c', we can perform the coercion
-- below.
runMagic :: forall c r . Magic c r -> StaticClass c -> r
runMagic = unsafeCoerce
{-# NOINLINE runMagic #-}

-- | Create a 'Static c' instance on the fly from a 'Closure (Dict c)'
withClosureDict :: forall c r . c => Closure (Dict c) -> (Static c => r) -> r
withClosureDict cDict k = runMagic (MkMagic k :: Magic c r) (MkStaticClass cDict)

-- | Create a 'Dict (Static c)' from a 'Dict c' and 'Closure (Dict c)'
mkStaticDict :: Dict c -> Closure (Dict c) -> Dict (Static c)
mkStaticDict Dict c = withClosureDict c Dict

-- | Using 'withClosureDict', we can ensure that 'Static c'
-- dictionaries are serializable. This is essential when we have
-- nested layers of remote execution.
instance (Typeable c, Static c) => Static (Static c) where
  closureDict = static mkStaticDict `ptrAp` closureDict @c `cAp` cPure (closureDict @c)
