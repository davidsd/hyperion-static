{-# LANGUAGE LambdaCase      #-}
{-# LANGUAGE StaticPointers  #-}
{-# LANGUAGE TemplateHaskell #-}

module Hyperion.Static.TH where

import           Control.Distributed.Static (Closure)
import           Data.Constraint            (Dict (Dict))
import           Data.Monoid                (Endo (Endo, appEndo))
import           Data.Typeable              (Typeable)
import           Hyperion.Static.Class      (cAp, cPtr)
import           Language.Haskell.TH        (Body (NormalB), Clause (Clause),
                                             Dec (FunD, InstanceD),
                                             Exp (AppE, ConE, InfixE, LamE, SigE, StaticE, VarE),
                                             Info (ClassI), InstanceDec, Name,
                                             Pat (ConP), Q,
                                             Type (AppT, ConT, ForallT, InfixT, ParensT, SigT, UInfixT, VarT),
                                             reify)
import qualified GHC.Generics
import qualified Data.Vector.Unboxed

-- | The code for this module is mostly copied from the static-closure package
-- https://hackage.haskell.org/package/static-closure-0.1.0.0/docs/Control-Static-Closure-TH.html

getAllInstances :: Name -> Q [InstanceDec]
getAllInstances className = do
  result <- reify className
  case result of
    ClassI _ instanceDecs -> pure instanceDecs
    _                     -> fail "getAllInstances: Not a class"

mkInstance :: Name -> Name -> InstanceDec -> InstanceDec
mkInstance getClosureDictName staticClassName instanceDec = case instanceDec of
  InstanceD maybeOverlap oldCxt oldType _ ->
    let
      dictTypeName = ''Dict
      dictType = ConT dictTypeName
      dictValueName = 'Dict
      dictValue = ConE dictValueName
      dictPat = ConP dictValueName [] []
      closureClassName = ''Closure
      closureClass = ConT closureClassName
      capName = 'cAp
      capValue = VarE capName
      getClosureDict = VarE getClosureDictName
      addClassF = AppT (ConT staticClassName)
      addTypeableF = AppT (ConT ''Typeable)
      fromStaticPtrName = 'cPtr
      fromStaticPtrExp = VarE fromStaticPtrName
      newType = addClassF oldType
      newStaticCxt = addClassF <$> oldCxt
      newTypeableCxt = (addTypeableF . VarT) <$> ((oldType:oldCxt) >>= findAllTypeVars)
      newCxt = newTypeableCxt ++ newStaticCxt
      mkTypeSig cxt = AppT closureClass (AppT dictType cxt)
      mkArgExp cxt = SigE getClosureDict (mkTypeSig cxt)
      addArg x cxt = InfixE (Just x) capValue (Just (mkArgExp cxt))
      funcPart = case (length oldCxt) of
        0 -> dictValue
        n -> LamE (replicate n dictPat) dictValue
      body = NormalB (foldl addArg (AppE fromStaticPtrExp (StaticE funcPart)) oldCxt)
      clause = Clause [] body []
      funClause = FunD getClosureDictName [clause]
    in
      InstanceD maybeOverlap newCxt newType [funClause]
  _ -> error "mkInstance: Not an instance"

mkInstancesIf :: (InstanceDec -> Bool) -> Name -> Name -> Name -> Q [InstanceDec]
mkInstancesIf instancePred getClosureDictName staticClassName className = do
  instances <- getAllInstances className
  pure $ fmap
    (mkInstance getClosureDictName staticClassName)
    (filter instancePred instances)

mkAllInstances :: Name -> Name -> Name -> Q [InstanceDec]
mkAllInstances = mkInstancesIf (const True)

findAllTypeVars :: Type -> [Name]
findAllTypeVars x = appEndo (go x) [] where
  go = \case
    ForallT{}       -> error "Don't know how do deal with Foralls in types"
    AppT t1 t2      -> go t1 <> go t2
    SigT t _        -> go t
    VarT name       -> Endo (name:)
    InfixT t1 _ t2  -> go t1 <> go t2
    UInfixT t1 _ t2 -> go t1 <> go t2
    ParensT t       -> go t
    _               -> mempty

-- Certain instances cannot be converted into Static instances with
-- the above code, so we need to filter them out before applying
-- 'mkInstance'. These filters are defined for use in Eq, Ord, and
-- Aeson.

-- | Ok for use with Eq and Ord
notGenerically1 :: InstanceDec -> Bool
notGenerically1 (InstanceD _ _ ty _)
  | AppT (ConT _) (AppT (AppT (ConT g) _) _) <- ty
  , g == ''GHC.Generics.Generically1
  = False
notGenerically1 _ = True

-- | Ok for use with FromJSON and ToJSON
okJSON :: InstanceDec -> Bool
okJSON (InstanceD _ _ ty _)
  | AppT (ConT _) (AppT (ConT g) _) <- ty
  , g == ''GHC.Generics.Generically
  = False
  | AppT (ConT _ ) (AppT (ConT v) _) <- ty
  , v == ''Data.Vector.Unboxed.Vector
  = False
okJSON _ = True
