{-# OPTIONS_GHC -fno-warn-orphans #-}
{-# OPTIONS_GHC -fno-warn-redundant-constraints #-}
{-# LANGUAGE DataKinds            #-}
{-# LANGUAGE FlexibleContexts     #-}
{-# LANGUAGE FlexibleInstances    #-}
{-# LANGUAGE KindSignatures       #-}
{-# LANGUAGE ScopedTypeVariables  #-}
{-# LANGUAGE StaticPointers       #-}
{-# LANGUAGE TemplateHaskell      #-}

module Hyperion.Static.Aeson where

import           Data.Aeson            (FromJSON, FromJSONKey, ToJSON,
                                        ToJSONKey)
import           Data.Map              ()
import           Data.Set              ()
import           Data.Vector           ()
import           Hyperion.Static.Class (Static (..))
import           Hyperion.Static.TH    (mkInstancesIf, mkAllInstances, okJSON)

mkInstancesIf okJSON 'closureDict ''Static ''FromJSON
mkAllInstances 'closureDict ''Static ''FromJSONKey
mkInstancesIf okJSON 'closureDict ''Static ''ToJSON
mkAllInstances 'closureDict ''Static ''ToJSONKey

